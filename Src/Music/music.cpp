#include "music.h"

#include "raylib.h"

#include "GameObjects/common_things.h"

using namespace gimpelGradius;

namespace gimpelGradius {
	static Music song;
	static bool noVolume = false;
	static float normalVolume = 1.0f;

	void initializeMusic() {
		song = LoadMusicStream("res/assets/snd/song.ogg");
		PlayMusicStream(song);
	}
	void updateMusic() {
		if (noVolume == false)
			UpdateMusicStream(song);

		if (IsKeyPressed(KEY_M)) {
			if (noVolume == false) {
				noVolume = true;
				SetMasterVolume(0);
			}
			else if (noVolume == true) {
				noVolume = false;
				SetMasterVolume(normalVolume);
			}
		}
	}
	void unloadGameSong() {
		UnloadMusicStream(song);
	}
}