#ifndef MUSIC_H
#define MUSIC_H

namespace gimpelGradius {
	void initializeMusic();
	void updateMusic();
	void unloadGameSong();
}

#endif