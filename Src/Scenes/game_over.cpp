#include "game_over.h"

#include "raylib.h"

#include "GameObjects/common_things.h"

using namespace gimpelGradius;

namespace gimpelGradius {
	static const float screenWidthDivisor = 3.0f;
	static const float screenHeightDivisor1 = 2.2f;
	static const float screenHeightDivisor2 = 1.8f;
	static const int fontSize = 25;
	static void keyInputs();

	void endMenu() {
		keyInputs();

		DrawTexture(map, 0, 0, WHITE);
		DrawText("Perdiste", static_cast<int>(screenWidth / screenWidthDivisor), static_cast<int>(screenHeight / screenHeightDivisor1), fontSize, WHITE);
		DrawText("Apriete ESCAPE para volver al menu", static_cast<int>(screenWidth / screenWidthDivisor), static_cast<int>(screenHeight / screenHeightDivisor2), fontSize, WHITE);
	}
	void keyInputs() {
		if (IsKeyPressed(KEY_ESCAPE)) {
			isOnMenu = true;
			isOnGameOver = false;
		}
	}
}