#include "controls.h"

#include "raylib.h"

#include "GameObjects/common_things.h"
#include "Scenes/game_menu.h"
#include "GameObjects/player.h"

using namespace gimpelGradius;

namespace gimpelGradius {
	static void controlsInputs();
	static const int posX1 = 425;
	static const int posX2 = 750;
	static const int posY1 = 100;
	static const int posY2 = 133;
	static const int posY3 = 166;
	static const int posY4 = 200;
	static const int fontSize = 25;
	static Color color = WHITE;

	void showControls() {
		controlsInputs();

		DrawTexture(map, 0, 0, WHITE);
		DrawText("Movimiento", 0, posY1, fontSize, color);
		DrawText("Izq: A Der: D", 0, posY2, fontSize, color);
		DrawText("Arriba: W  Abajo: S", 0, posY3, fontSize, color);

		DrawText("Volver al menu", posX1, posY1, fontSize, color);
		DrawText("Retroceso <----", posX1, posY2, fontSize, color);

		DrawText("Pausar el juego: P", posX2, posY1, fontSize, color);
		DrawText("Sacar los sonidos: M", posX2, posY2, fontSize, color);
		DrawText("Disparar: ESPACIO", posX2, posY3, fontSize, color);
		DrawText("Bombas: B", posX2, posY4, fontSize, color);
	}
	void controlsInputs() {
		if (IsKeyDown(KEY_BACKSPACE)) {
			isOnControls = false;
			isOnMenu = true;
		}
	}
}