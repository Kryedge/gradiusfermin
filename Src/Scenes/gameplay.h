#ifndef GAMEPLAY_H
#define GAMEPLAY_H

namespace gimpelGradius {
	void gameplay();
	void drawMap();
	void moveMap();
	void keyInputs();
}

#endif