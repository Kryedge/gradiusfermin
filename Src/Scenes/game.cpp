#include "game.h"

#include "raylib.h"

#include "GameObjects/common_things.h"
#include "Scenes/game_menu.h"
#include "Scenes/gameplay.h"
#include "Scenes/game_over.h"
#include "Scenes/credits.h"
#include "Scenes/controls.h"
#include "Music/music.h"
#include "GameObjects/enemy.h"
#include "GameObjects/player.h"
#include "GameObjects/player_shoot.h"
#include "GameObjects/enemy_shoot.h"

using namespace gimpelGradius;

namespace gimpelGradius {
	static bool closeGame();
	static void loadGameTextures();
	static void unloadThings();
	static const int versionFontSize = 25;

	void gameLoop() {
		InitWindow(screenWidth, screenHeight, "Gimpel Gardius");
		loadGameTextures();
		InitAudioDevice();
		initializeMusic();

		SetTargetFPS(60);
		SetExitKey(KEY_VOLUME_DOWN);

		while (!WindowShouldClose() && closeGame() == false) {
			BeginDrawing();
			ClearBackground(BLACK);
			updateMusic();

			if (isOnMenu == true)
				showMenu();
			else if (isOnGameplay == true)
				gameplay();
			else if (isOnGameOver == true)
				endMenu();
			else if (isOnCredits == true)
				showCredits();
			else if (isOnControls == true)
				showControls();

			DrawText(gameVersion, 0, 0, versionFontSize, WHITE);
			EndDrawing();
		}
		unloadThings();
		CloseWindow();
	}
	bool closeGame() {
		if (isOnMenu || isOnGameplay || isOnGameOver || isOnCredits || isOnControls)
			return false;

		return true;
	}
	void loadGameTextures() {
		map = LoadTexture("res/assets/img/map.jpg");
		map2 = LoadTexture("res/assets/img/map2.jpg");
		mapFront = LoadTexture("res/assets/img/mapFront.png");
		mapFront2 = LoadTexture("res/assets/img/mapFront2.png");
		loadPlayerAssets();
		loadEnemySprites();
		loadPlayerShootAssets();
		loadEnemyShootAssets();
	}
	void unloadThings() {
		UnloadTexture(map);
		UnloadTexture(map2);
		UnloadTexture(mapFront);
		UnloadTexture(mapFront2);
		unloadPlayerAssets();
		unloadEnemySprites();
		unloadEnemyShootAssets();
		unloadPlayerShootAssets();

		unloadGameSong();
	}
}