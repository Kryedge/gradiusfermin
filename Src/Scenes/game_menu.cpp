#include "game_menu.h"

#include "raylib.h"

#include "GameObjects/common_things.h"
#include "GameObjects/player.h"
#include "GameObjects/enemy.h"
#include "GameObjects/player_shoot.h"
#include "GameObjects/enemy_shoot.h"

using namespace gimpelGradius;

namespace gimpelGradius {
	static const float screenWidthDivisor1 = 2.8f;
	static const float screenWidthDivisor2 = 3.1f;
	static const float screenHeightDivisorLess = 0.3f;
	static const float screenHeightDivisorTitle = 7.5f;
	static const float screenHeightDivisor1 = 2.2f;
	static const float screenHeightDivisor2 = screenHeightDivisor1 - screenHeightDivisorLess;
	static const float screenHeightDivisor3 = screenHeightDivisor2 - screenHeightDivisorLess;
	static const float screenHeightDivisorExit = 1.4f;
	static const int fontSize = 20;
	static const int titleFontSize = fontSize * 2;
	static const Color color = WHITE;
	static void keyInputs();

	void showMenu() {
		keyInputs();

		DrawTexture(map, 0, 0, color);
		DrawText("GARDIUS", static_cast<int>(screenWidth / screenWidthDivisor1), static_cast<int>(screenHeight / screenHeightDivisorTitle), titleFontSize, color);
		DrawText("Apriete ENTER para empezar", static_cast<int>(screenWidth / screenWidthDivisor2), static_cast<int>(screenHeight / screenHeightDivisor1), fontSize, color);
		DrawText("Apriete 8 para ir a los creditos", static_cast<int>(screenWidth / screenWidthDivisor2), static_cast<int>(screenHeight / screenHeightDivisor2), fontSize, color);
		DrawText("Apriete 9 para ver los controles", static_cast<int>(screenWidth / screenWidthDivisor2), static_cast<int>(screenHeight / screenHeightDivisor3), fontSize, color);
		DrawText("Apriete ESCAPE para salir del juego", static_cast<int>(screenWidth / screenWidthDivisor2), static_cast<int>(screenHeight / screenHeightDivisorExit), fontSize, color);
	}

	void keyInputs() {
		if (IsKeyDown(KEY_ENTER)) {
			isOnMenu = false;
			isOnGameplay = true;
			enemiesInitialized = false;
			isPlayerInitialized = false;
			isShootInitialized = false;
			isEnemiesShootInitialized = false;
		}
		else if (IsKeyDown(KEY_EIGHT)) {
			isOnMenu = false;
			isOnCredits = true;
		}
		else if (IsKeyDown(KEY_NINE)) {
			isOnMenu = false;
			isOnControls = true;
		}
		else if (IsKeyPressed(KEY_ESCAPE)) {
			isOnMenu = false;
		}
	}
}
