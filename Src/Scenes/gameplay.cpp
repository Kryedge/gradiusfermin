#include "gameplay.h"

#include "raylib.h"

#include "GameObjects/common_things.h"
#include "GameObjects/player.h"
#include "GameObjects/enemy.h"
#include "GameObjects/player_shoot.h"
#include "GameObjects/enemy_shoot.h"

using namespace gimpelGradius;

namespace gimpelGradius {
	static const int mapWidth = 1366;
	static const int mapFrontWidth = 2700;
	static const int negativeMapWidth = -1366;
	static float mapPosX = 0;
	static float mapFrontPosX = 0;
	static float mapFront2PosX = mapFrontWidth;
	static float map2PosX = mapWidth;
	static float mapPosMove = 200.0f;
	static bool returnMap = false;
	static bool gamePaused = false;
	static int fontSize = 50;

	void gameplay() {
		keyInputs();
		if (gamePaused == false) {
			movePlayer();
			shoot();

			createEnemy();
			moveEnemy();
			moveShoot();
			enemiesShoot();
			moveEnemiesShoot();
		}
		drawMap();
	}
	void drawMap() {
		if (gamePaused == false)
			moveMap();

		DrawTexture(map, static_cast<int>(mapPosX), 0, WHITE);
		DrawTexture(map2, static_cast<int>(map2PosX), 0, WHITE);
		DrawTexture(mapFront, static_cast<int>(mapFrontPosX), 0, WHITE);
		DrawTexture(mapFront, static_cast<int>(mapFront2PosX), 0, WHITE);
		drawPlayer();
		drawEnemy();
		drawShoot();
		drawEnemiesShoot();

		if (gamePaused == true)
			DrawText("PAUSED", static_cast<int>(screenWidth / 3), static_cast<int>(screenHeight / 2), fontSize, WHITE);
	}
	void moveMap() {
		mapPosX -= mapPosMove * GetFrameTime();
		mapFrontPosX -= (mapPosMove/4) * GetFrameTime();
		mapFront2PosX -= (mapPosMove/4) * GetFrameTime();
		map2PosX -= mapPosMove * GetFrameTime();

		if (mapPosX <= negativeMapWidth)
			mapPosX = mapWidth;

		if (map2PosX <= negativeMapWidth)
			map2PosX = mapWidth;	
		
		if (mapFrontPosX <= (-mapFrontWidth))
			mapFrontPosX = mapFrontWidth;

		if (mapFront2PosX <= (-mapFrontWidth))
			mapFront2PosX = mapFrontWidth;
	}
	void keyInputs() {
		if (IsKeyPressed(KEY_P)) {
			if (gamePaused == true)
				gamePaused = false;
			else if (gamePaused == false)
				gamePaused = true;
		}

		if (IsKeyPressed(KEY_BACKSPACE)) {
			isOnGameplay = false;
			isOnMenu = true;
			gamePaused = false;
		}
	}
}