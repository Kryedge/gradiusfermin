#include "credits.h"

#include "raylib.h"

#include "GameObjects/common_things.h"
#include "Scenes/game_menu.h"

using namespace gimpelGradius;

namespace gimpelGradius {
	static void creditsInputs();
	static const float screenWidthDivisorTitle = 2.5f;
	static const float screenWidthDivisor = 3.4f;
	static const int screenHeightExtra = 33;
	static const float screenHeightDivisorTitle = 8.0f;
	static const int screenHeight1 = 70;
	static const int screenHeight2 = screenHeight1 + screenHeightExtra;
	static const int screenHeight3 = screenHeight2 + screenHeightExtra;
	static const int screenHeight4 = screenHeight3 + screenHeightExtra;
	static const int screenHeight5 = screenHeight4 + screenHeightExtra;
	static const int screenHeight6 = screenHeight5 + screenHeightExtra;
	static const int screenHeight7 = screenHeight6 + (screenHeightExtra * 2);
	static const int screenHeight8 = screenHeight6 + screenHeightExtra;
	static const int fontSize = 20;
	static const Color color = WHITE;

	void showCredits() {
		creditsInputs();
		
		DrawTexture(map, 0, 0, WHITE);
		DrawText("CREDITOS", static_cast<int>(screenWidth / screenWidthDivisorTitle), static_cast<int>(screenHeight / screenHeightDivisorTitle), fontSize, color);
		DrawText("Juego hecho por Fermin Gimpel", static_cast<int>(screenWidth / screenWidthDivisor), screenHeight1, fontSize, color);
		DrawText("Sonidos hechos por Fermin Gimpel", static_cast<int>(screenWidth / screenWidthDivisor), screenHeight2, fontSize, color);
		DrawText("Texturas hechas por Fermin Gimpel", static_cast<int>(screenWidth / screenWidthDivisor), screenHeight3, fontSize, color);
		DrawText("Musica: TeknoAXE - Dark Versus Light", static_cast<int>(screenWidth / screenWidthDivisor), screenHeight4, fontSize, color);
		DrawText("https://youtu.be/uxD88mxMDQc", static_cast<int>(screenWidth / screenWidthDivisor), screenHeight5, fontSize, color);
		DrawText("Programas usados Piskel - Photoshop - BFXR", static_cast<int>(screenWidth / screenWidthDivisor), screenHeight6, fontSize, color);
		DrawText("Volver al menu", 0, screenHeight5, fontSize, color);
		DrawText("Retroceso <------", 0, screenHeight6, fontSize, color);
		DrawText("Funciones Extra y Enemigo Terrestre por Tomas Carceglia", static_cast<int>(screenWidth / screenWidthDivisor), screenHeight8, fontSize, color);
	}
	void creditsInputs() {
		if (IsKeyDown(KEY_BACKSPACE)) {
			isOnCredits = false;
			isOnMenu = true;
		}
	}
}