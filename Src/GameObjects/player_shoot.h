#ifndef PLAYER_SHOOT_H
#define PLAYER_SHOOT_H

#include "raylib.h"

namespace gimpelGradius {
	struct Shoot {
		Rectangle body;
		Texture2D sprite;
		bool isShooted;
	};

	extern bool isShootInitialized;

	void shoot();
	void moveShoot();
	void drawShoot();
	void loadPlayerShootAssets();
	void unloadPlayerShootAssets();

	extern Shoot playerShoot;
}

#endif