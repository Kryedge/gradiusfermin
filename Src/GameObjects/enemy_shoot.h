#ifndef ENEMY_SHOOT_H
#define ENEMY_SHOOT_H

#include "raylib.h"

namespace gimpelGradius {
	struct enemiesAttack {
		Rectangle body;
		Texture2D sprite;
		bool isShooted;
	};

	extern Texture2D enemyShootSprite;
	extern bool isEnemiesShootInitialized;

	void initializeEnemiesShoot();
	void enemiesShoot();
	void moveEnemiesShoot();
	void drawEnemiesShoot();
	void loadEnemyShootAssets();
	void unloadEnemyShootAssets();

	extern enemiesAttack enemyShoot[];
}

#endif