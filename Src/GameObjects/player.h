#ifndef PLAYER_H
#define PLAYER_H

#include "raylib.h"

namespace gimpelGradius {
	struct player {
		Rectangle playerPosition;
		int playerLives;
		bool isAlive;
		Texture2D sprite;
		int playerPoints;
	};

	extern const int pointsPerEnemy;
	extern bool isPlayerInitialized;

	void initializePlayer();
	void movePlayer();
	void killPlayer();
	void drawPlayer();
	void loadPlayerAssets();
	void unloadPlayerAssets();

	extern player player1;
}

#endif