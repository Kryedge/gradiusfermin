#ifndef COMMON_THINGS_H
#define COMMON_THINGS_H

#include "raylib.h"

namespace gimpelGradius {
	extern int screenWidth;
	extern int screenHeight;

	extern bool isOnMenu;
	extern bool isOnGameplay;
	extern bool isOnGameOver;
	extern bool isOnCredits;
	extern bool isOnControls;
	extern bool win;
	extern bool lose;
	extern Texture2D map;
	extern Texture2D map2;
	extern Texture2D mapFront;
	extern Texture2D mapFront2;

	extern const char gameVersion[];
}

#endif
