#ifndef ENEMY_H
#define ENEMY_H

#include "raylib.h"

namespace gimpelGradius {
	struct enemy {
		Rectangle enemyPosition;
		bool isAlive;
		Texture2D sprite;
	};

	extern bool enemiesInitialized;
	extern const int maxEnemies;
	extern Texture2D enemiesTexures[];
	extern const int landEnemy;
	void createEnemy();
	void moveEnemy();
	void drawEnemy();
	void loadEnemySprites();
	void unloadEnemySprites();

	extern enemy enemies[];
}

#endif