#include "enemy.h"

#include "raylib.h"

#include "GameObjects/common_things.h"
#include "GameObjects/player.h"
#include "GameObjects/player_shoot.h"

using namespace gimpelGradius;


namespace gimpelGradius {

	bool enemiesInitialized = false;

	static int actualEnemies = 0;

	extern const int landEnemy = 15;
	static const float enemiesHitBox = 48;
	static const float enemiesSpeedX = 300.0f;
	static const float enemiesSpeedY = 100.0f;

	static bool texturesLoaded = false;

	static const int maxEnemiesSprites = 4;
	const int maxEnemies = 16;
	enemy enemies[maxEnemies];
	Texture2D enemiesTexures[maxEnemiesSprites];

	static float secondsToNewEnemy = 0.66f;
	static float timer = 0.0f;


	void createEnemy() {

		if (enemiesInitialized == false) {
			for (int i = 0; i < maxEnemies-1; i++) {
				enemies[i].enemyPosition.x = screenWidth + enemiesHitBox;
				enemies[i].enemyPosition.y = screenHeight + enemiesHitBox;
				enemies[i].isAlive = false;
			}
			enemies[landEnemy].enemyPosition.x = screenWidth + enemiesHitBox;
			enemies[landEnemy].enemyPosition.y = 10;
			enemies[landEnemy].isAlive = false;

			enemiesInitialized = true;
		}
		
		if (texturesLoaded == false) {
			for (int i = 0; i < maxEnemies-1; i++) {
				int enemySprite = GetRandomValue(0, maxEnemiesSprites-1);
				switch (enemySprite) {
				case 0:
					enemies[i].sprite = enemiesTexures[enemySprite];
					break;
				case 1:
					enemies[i].sprite = enemiesTexures[enemySprite];
					break;
				case 2:
					enemies[i].sprite = enemiesTexures[enemySprite];
					break;
				}
			}
			enemies[landEnemy].sprite = enemiesTexures[3];

			texturesLoaded = true;
		}

		timer += GetFrameTime();
		if (timer >= secondsToNewEnemy) {
			enemies[actualEnemies].enemyPosition.x = static_cast<float>(screenWidth);
			enemies[actualEnemies].enemyPosition.y = static_cast<float>(GetRandomValue(0, static_cast<int>(screenHeight - enemiesHitBox)));
			enemies[actualEnemies].enemyPosition.height = enemiesHitBox;
			enemies[actualEnemies].enemyPosition.width = enemiesHitBox;
			enemies[actualEnemies].isAlive = true;
			actualEnemies++;
			if (actualEnemies >= maxEnemies)
				actualEnemies = 0;
		
			enemies[landEnemy].enemyPosition.y = 300;

			timer = 0.0f;
		}
	}
	void moveEnemy() {
		for (int i = 0; i < maxEnemies-1; i++) {
			if (enemies[i].isAlive == true) {
				if (static_cast<int>(player1.playerPosition.y) < static_cast<int>(enemies[i].enemyPosition.y))
					enemies[i].enemyPosition.y -= enemiesSpeedY * GetFrameTime();
				else if (static_cast<int>(player1.playerPosition.y) > static_cast<int>(enemies[i].enemyPosition.y))
					enemies[i].enemyPosition.y += enemiesSpeedY * GetFrameTime();
				else if (static_cast<int>(player1.playerPosition.y) == static_cast<int>(enemies[i].enemyPosition.y))
					enemies[i].enemyPosition.y = player1.playerPosition.y;

				enemies[i].enemyPosition.x -= enemiesSpeedX * GetFrameTime();
			}


			if (CheckCollisionRecs(enemies[i].enemyPosition, player1.playerPosition) && enemies[i].isAlive == true) {
				player1.playerLives--;
				enemies[i].isAlive = false;
				if (player1.playerLives <= 0) {
					player1.isAlive = false;
					isOnGameplay = false;
					isOnGameOver = true;
				}
			}
		}
		enemies[landEnemy].enemyPosition.x -= enemiesSpeedX * GetFrameTime();
	}
	void drawEnemy() {
		for (int i = 0; i < maxEnemies; i++) {
			if (enemies[i].isAlive == true)
				DrawTexture(enemies[i].sprite, static_cast<int>(enemies[i].enemyPosition.x), static_cast<int>(enemies[i].enemyPosition.y), WHITE);
		}
	}
	void loadEnemySprites() {
		enemiesTexures[0] = LoadTexture("res/assets/img/enemy1.png");
		enemiesTexures[1] = LoadTexture("res/assets/img/enemy2.png");
		enemiesTexures[2] = LoadTexture("res/assets/img/enemy3.png");
		enemiesTexures[3] = LoadTexture("res/assets/img/landenemy.png");
	}
	void unloadEnemySprites() {
		for (int i = 0; i < maxEnemiesSprites; i++)
			UnloadTexture(enemiesTexures[i]);
		for (int i = 0; i < maxEnemiesSprites; i++)
			UnloadTexture(enemies[i].sprite);
	}
}