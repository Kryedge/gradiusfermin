#include "enemy_shoot.h"

#include "raylib.h"

#include "GameObjects/common_things.h"
#include "GameObjects/player.h"
#include "GameObjects/enemy.h"

using namespace gimpelGradius;

namespace gimpelGradius {
	static const int maxShoots = 16;
	static const float shootWidth = 20;
	static const float shootHeight = 10;
	static const float shootSpeed = 450.0f;

	static float shootTimer = 0.0f;
	static const float timeToShoot = 0.5f;
	static const int shootProbability = 50;

	static const float enemyShootSoundVolume = 0.33f;
	static Sound enemyShootSound;
	Texture2D enemyShootSprite;
	enemiesAttack enemyShoot[maxShoots];
	bool isEnemiesShootInitialized = false;

	void initializeEnemiesShoot() {
		for (int i = 0; i < maxShoots; i++) {
			enemyShoot[i].body.width = shootWidth;
			enemyShoot[i].body.height = shootHeight;
			enemyShoot[i].isShooted = false;
			enemyShoot[i].sprite = enemyShootSprite;
		}
		isEnemiesShootInitialized = true;
	}
	void enemiesShoot() {
		if (isEnemiesShootInitialized == false)
			initializeEnemiesShoot();

		shootTimer += GetFrameTime();
		if (shootTimer >= timeToShoot) {
			shootTimer = 0;

			for (int i = 0; i < (maxShoots-1); i++) {
				if (enemies[i].isAlive == true && enemyShoot[i].isShooted == false) {
					if (GetRandomValue(0, 100) >= shootProbability) {
						enemyShoot[i].body.x = enemies[i].enemyPosition.x;
						enemyShoot[i].body.y = enemies[i].enemyPosition.y + (enemies[i].enemyPosition.height / 2);
						enemyShoot[i].isShooted = true;
						PlaySound(enemyShootSound);
					}
				}
				if (enemies[landEnemy].isAlive && enemyShoot[maxShoots-1].isShooted == false)
				{
					if (GetRandomValue(0, 100) >= shootProbability)
					{
						enemyShoot[maxShoots-1].body.x = enemies[landEnemy].enemyPosition.x;
						enemyShoot[maxShoots-1].body.y = enemies[landEnemy].enemyPosition.y + (enemies[landEnemy].enemyPosition.height / 2);
						enemyShoot[maxShoots-1].isShooted = true;
						PlaySound(enemyShootSound);
					}
				}
			}
		
		}
	}
	void moveEnemiesShoot() {
		for (int i = 0; i < (maxShoots-1); i++) {
			if (enemyShoot[i].isShooted == true) {
				enemyShoot[i].body.x -= shootSpeed * GetFrameTime();

				if (enemyShoot[i].body.x <= 0)
					enemyShoot[i].isShooted = false;

				if (CheckCollisionRecs(enemyShoot[i].body, player1.playerPosition)) {
					player1.playerLives--;
					enemyShoot[i].isShooted = false;
					if (player1.playerLives <= 0) {
						killPlayer();
					}
				}
			}
		}
		if (enemyShoot[maxShoots-1].isShooted == true) {
			enemyShoot[maxShoots - 1].body.x -= shootSpeed * GetFrameTime();
			enemyShoot[maxShoots - 1].body.y -= (shootSpeed/3) * GetFrameTime();

			if (enemyShoot[maxShoots - 1].body.x <= 0)
				enemyShoot[maxShoots - 1].isShooted = false;

			if (CheckCollisionRecs(enemyShoot[maxShoots - 1].body, player1.playerPosition)) {
				player1.playerLives--;
				enemyShoot[maxShoots - 1].isShooted = false;
				if (player1.playerLives <= 0) {
					killPlayer();
				}
			}
		}
	}
	void drawEnemiesShoot() {
		for (int i = 0; i < maxShoots; i++) {
			if (enemyShoot[i].isShooted == true)
				DrawTexture(enemyShoot[i].sprite, static_cast<int>(enemyShoot[i].body.x), static_cast<int>(enemyShoot[i].body.y), WHITE);
		}
	}
	void loadEnemyShootAssets() {
		enemyShootSprite = LoadTexture("res/assets/img/enemyShoot.png");
		enemyShootSound = LoadSound("res/assets/snd/EnemyShoot.wav");
		SetSoundVolume(enemyShootSound, enemyShootSoundVolume);
	}
	void unloadEnemyShootAssets() {
		for (int i = 0; i < maxShoots; i++) {
			UnloadTexture(enemyShoot[i].sprite);
		}
		UnloadTexture(enemyShootSprite);
		UnloadSound(enemyShootSound);
	}
}