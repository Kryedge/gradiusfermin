#include "player.h"

#include "raylib.h"

#include "GameObjects/common_things.h"
#include "GameObjects/enemy.h"
#include "GameObjects/player_shoot.h"

using namespace gimpelGradius;

namespace gimpelGradius {

	bool isPlayerInitialized = false;
	const int pointsPerEnemy = 25;

	static const int playerHitBox = 48;
	static const float playerSpeed = 375.0f;
	static const int initialPlayerLives = 3;
	player player1;
	static Sound playerDeathSound;
	
	static const int fontSize = 25;
	static const int playerPointsX = 100;

	static Vector2 position;
	static Rectangle frameRec;
	static float currentFrame = 0;
	static float framesCounter = 0;

	static float maxCounter = 0.5f;

	void initializePlayer() {
		player1.playerPosition.height = playerHitBox;
		player1.playerPosition.width = playerHitBox;
		player1.playerPosition.x = playerHitBox;
		player1.playerPosition.y = static_cast<float>(screenHeight / 2);
		player1.isAlive = true;
		player1.playerLives = initialPlayerLives;
		player1.playerPoints = 0;
		
		isPlayerInitialized = true;

		frameRec = { 0.0f, 0.0f, static_cast<float>(player1.sprite.width), static_cast<float>(player1.sprite.height / 2) };
	}

	void movePlayer() {
		if (isPlayerInitialized == false)
			initializePlayer();

		if (IsKeyDown(KEY_W)) {
			player1.playerPosition.y -= playerSpeed * GetFrameTime();
			if (player1.playerPosition.y < 0)
				player1.playerPosition.y = 1;
		}
		else if (IsKeyDown(KEY_S)) {
			player1.playerPosition.y += playerSpeed * GetFrameTime();
			if (player1.playerPosition.y > (screenHeight - player1.playerPosition.height))
				player1.playerPosition.y = screenHeight - player1.playerPosition.height;
		}

		if (IsKeyDown(KEY_D)) {
			player1.playerPosition.x += playerSpeed * GetFrameTime();
			if (player1.playerPosition.x > (screenWidth / 2) - player1.playerPosition.width)
				player1.playerPosition.x = static_cast<float>((screenWidth / 2) - player1.playerPosition.width);
		}
		else if (IsKeyDown(KEY_A)) {
			player1.playerPosition.x -= playerSpeed * GetFrameTime();
			if (player1.playerPosition.x < 0)
				player1.playerPosition.x = 1;
		}
	}
	void killPlayer() {
		player1.isAlive = false;
		isOnGameplay = false;
		isOnGameOver = true;
		PlaySound(playerDeathSound);
	}
	void drawPlayer() {
		DrawText(TextFormat("HP:%i", player1.playerLives), 0, static_cast<int>(screenHeight - fontSize), fontSize, RED);
		DrawText(TextFormat("Points:%i", player1.playerPoints), playerPointsX, static_cast<int>(screenHeight - fontSize), fontSize, RED);
		framesCounter += GetFrameTime();

		if (framesCounter >= (maxCounter))
		{
			framesCounter = 0;

			currentFrame++;
			if (currentFrame > 1)
				currentFrame = 0;

			frameRec.y = static_cast<float>(currentFrame*(player1.sprite.height / 2));
		}
		position = { player1.playerPosition.x, player1.playerPosition.y };

		DrawTextureRec(player1.sprite, frameRec, position, WHITE);

	}
	void loadPlayerAssets() {
		player1.sprite = LoadTexture("res/assets/img/player.png");
		playerDeathSound = LoadSound("res/assets/snd/PlayerExplosion.wav");
	}
	void unloadPlayerAssets() {
		UnloadTexture(player1.sprite);
		UnloadSound(playerDeathSound);
	}
}