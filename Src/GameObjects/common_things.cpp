#include "common_things.h"

#include "raylib.h"

using namespace gimpelGradius;
namespace gimpelGradius {
	int screenWidth = 1024;
	int screenHeight = 360;

	bool isOnMenu = true;
	bool isOnGameplay = false;
	bool isOnGameOver = false;
	bool isOnCredits = false;
	bool isOnControls = false;
	bool win = false;
	bool lose = false;

	Texture2D map;
	Texture2D map2;
	Texture2D mapFront;
	Texture2D mapFront2;

	const char gameVersion[5] = { 'V',' ','0','.','4' };
}
