#include "player_shoot.h"

#include "raylib.h"

#include "GameObjects/common_things.h"
#include "GameObjects/player.h"
#include "GameObjects/enemy.h"

using namespace gimpelGradius;

namespace gimpelGradius {
	bool isShootInitialized = false;

	Shoot playerShoot;
	Shoot playerBomb;
	Sound playerShootSound;
	static const float shootWidth = 20;
	static const float shootHeight = 10;
	static const float shootSpeed = 250.0f;
	static void initializeShoot();
	
	void initializeShoot() {
		playerShoot.body.width = shootWidth;
		playerBomb.body.width = shootWidth;
		playerShoot.body.height = shootHeight;
		playerBomb.body.height = shootHeight;
		playerShoot.isShooted = false;
		playerBomb.isShooted = false;
		isShootInitialized = true;
	}
	void shoot() {
		if ((IsKeyPressed(KEY_SPACE) || IsKeyPressed(KEY_ENTER)) && playerShoot.isShooted == false) {
			int halfPlayerY = static_cast<int>(player1.playerPosition.y + (player1.playerPosition.height / 2 - playerShoot.body.height / 2));
			int playerXMax = static_cast<int>(player1.playerPosition.x + player1.playerPosition.width);
			playerShoot.body.x = static_cast<float>(playerXMax);
			playerShoot.body.y = static_cast<float>(halfPlayerY);
			playerShoot.isShooted = true;
			PlaySound(playerShootSound);
			DrawRectangle(playerXMax, halfPlayerY, static_cast<int>(playerShoot.body.width), static_cast<int>(playerShoot.body.height), RED);
		}
		if (IsKeyPressed(KEY_B) && playerBomb.isShooted == false)
		{
			int halfPlayerY = static_cast<int>(player1.playerPosition.y + (player1.playerPosition.height / 2 - playerShoot.body.height / 2));
			int playerXMax = static_cast<int>(player1.playerPosition.x + player1.playerPosition.width);
			playerBomb.body.x = static_cast<float>(playerXMax);
			playerBomb.body.y = static_cast<float>(halfPlayerY);
			playerBomb.isShooted = true;
			PlaySound(playerShootSound);
			DrawRectangle(playerXMax, halfPlayerY, static_cast<int>(playerBomb.body.width), static_cast<int>(playerBomb.body.height), RED);
		}
	}
	void moveShoot() {
		if (isShootInitialized == false)
			initializeShoot();

		if (playerShoot.isShooted == true) {

			playerShoot.body.x += shootSpeed * GetFrameTime();
			if (playerShoot.body.x >= screenWidth)
				playerShoot.isShooted = false;

			for (int i = 0; i < maxEnemies; i++) {
				if (CheckCollisionRecs(playerShoot.body, enemies[i].enemyPosition) && enemies[i].isAlive == true) {
					enemies[i].isAlive = false;
					playerShoot.isShooted = false;
					player1.playerPoints += pointsPerEnemy;
				}

			}
		}
		
		if (playerBomb.isShooted == true) 
		{
			playerBomb.body.x += (shootSpeed/2) * GetFrameTime();
			playerBomb.body.y += (shootSpeed/2) * GetFrameTime();
			if (playerBomb.body.x >= screenWidth || playerBomb.body.y >= screenHeight)
				playerBomb.isShooted = false;

			for (int i = 0; i < maxEnemies; i++) {
				if (CheckCollisionRecs(playerBomb.body, enemies[i].enemyPosition) && enemies[i].isAlive == true) {
					enemies[i].isAlive = false;
					playerBomb.isShooted = false;
					player1.playerPoints += pointsPerEnemy;
				}

			}
		}
	}
	void drawShoot() {
		if (playerShoot.isShooted == true)
			DrawTexture(playerShoot.sprite, static_cast<int>(playerShoot.body.x), static_cast<int>(playerShoot.body.y), WHITE);

		if (playerBomb.isShooted)
		{
			DrawTexture(playerBomb.sprite, static_cast<int>(playerBomb.body.x), static_cast<int>(playerBomb.body.y), WHITE);
		}
	}
	void loadPlayerShootAssets() {
		playerShoot.sprite = LoadTexture("res/assets/img/playerShoot.png");
		playerShootSound = LoadSound("res/assets/snd/PlayerShoot.wav");
		playerBomb.sprite = LoadTexture("res/assets/img/bomb.png");
	}
	void unloadPlayerShootAssets() {
		UnloadTexture(playerShoot.sprite);
		UnloadTexture(playerBomb.sprite);
		UnloadSound(playerShootSound);
	}

}