﻿![](https://i.imgur.com/XF8KOlb.jpg)

# Saludos!

### Mi nombre es Tomas Carceglia, soy un Game Designer y Programador Argentino, este es mi 4º juego.
### Es una nueva versión de Gradius hecha por Fermin Gimpel y modificada posteriormente por mi (enemigos terrestres y bombas del jugador).
# Imagenes del juego
![](https://i.imgur.com/l6TFMuq.png)
![](https://i.imgur.com/whHDORH.png)
![](https://i.imgur.com/kIOpgzH.png)
![](https://i.imgur.com/10k89aQ.png)

# Mail
## kryedge@gmail.com
## https://itch.io/profile/kryedge
